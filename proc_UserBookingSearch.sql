    
Alter PROCEDURE [dbo].[proc_UserBookingSearch]                                
(                              
 @UserID INT                                
)                              
AS                                
                                
SELECT  TOP 20 [Bookings].Id, [Bookings].Name, [Bookings].BookingStart, [Bookings].BookingEnd, [Bookings].Amount,        
 [Bookings].CreatedDate, ISNULL([Bookings].Cancelled, 0) Cancelled,                   
 [Bookings].CancelledDate,  [Bookings].UserId,   [Bookings].[Status] ,  [Bookings].[LicPlate],                
 CASE               
  --Find bookings before 12 hours of startdate and not cancelled                    
  WHEN  DATEDIFF (hh,  (SELECT [dbo].[dReturnDate]( GetUTCdate())), [Bookings].BookingStart) > 12 AND ISNULL([Bookings].CancelledTransactionID, '') = '' THEN 'REFUND'                            
 ELSE ''                              
 END Refundable,          
 CASE          
  WHEN [Bookings].Amount = 0 THEN [Bookings].TransactionResponse           
 ELSE ''          
 END payType    ,        
 PA.[PricingInfo]  ,      
 PT.Recurring,    
    [Bookings].BookingStartLabel,    
 [Bookings].BookingEndLabel              
FROM Bookings           
INNER JOIN ResourcePricings RP ON RP.Id = Bookings.ResourcePricingId        
INNER JOIN PricingAvailabilities PA ON PA.Id = RP.PricingAvailabilityId             
INNER JOIN PricingTypes PT ON PT.Id = PA.PricingTypeId                        
WHERE UserID = @UserID     AND  (Bookings.[Status] = 'Booked' OR Bookings.[Status] = 'Cancelled')                        
ORDER BY Bookings.BookingStart DESC