  
ALTER  PROCEDURE [dbo].[proc_BookingInitInsert]                
@Name VARCHAR(255),                
@ResourcePricingId  INT,                
@UserId INT,                
@GuestId INT = 0,                
@BookingDate DATETIME,                
@BookingStart DATETIME,                
@BookingEnd DATETIME,                
@Amount DECIMAL(18,2),            
@CreatedBy INT,            
@Status VARCHAR(50),      
@PromoCodeId Int,  
@LicPlate varchar(40)              
AS                
    
DECLARE @ErrorMsg VARCHAR(500)    
    
SET @ErrorMsg = ''    
    
BEGIN TRANSACTION TranBookingInsert    
    
BEGIN TRY    
      
-- At this point @ResourcePricingId may not available. check again here    
DECLARE @NewResourcePricingId  INT                                  
CREATE TABLE #tmpAllAvailableResources (                                        
 tmpID int IDENTITY(1,1) NOT NULL,                                         
 tmpResourceId int,    
 tmpResourcePricingId int    
)      
    
INSERT INTO #tmpAllAvailableResources    
SELECT ResourceID, Id FROM ResourcePricings Where PricingAvailabilityID =( SELECT PricingAvailabilityID  From ResourcePricings where Id =@ResourcePricingId)    
    
SET @NewResourcePricingId = (SELECT TOP 1 tmpResourcePricingId  FROM #tmpAllAvailableResources WHERE tmpResourceID NOT IN (    
SELECT DISTINCT RP.ResourceID FROM ResourcePricings RP        
INNER JOIN Bookings B ON B.ResourcePricingId = RP.Id         
AND (                      
   (BookingStart BETWEEN @BookingStart AND  @BookingEnd)                                      
   OR (BookingEnd BETWEEN @BookingStart AND  @BookingEnd)                      
   OR (@BookingStart BETWEEN BookingStart AND  BookingEnd)                                      
   OR (@BookingEnd BETWEEN BookingStart AND  BookingEnd))                      
AND (B.[Status]='Booked' OR B.[Status]='Pending')   ))     
    
DROP TABLE #tmpAllAvailableResources    
----------------------------------------------------------------    
    
IF @NewResourcePricingId > 0    
BEGIN    
 INSERT INTO Bookings                 
  ([Name]                
  ,[ResourcePricingId]                
  ,[UserId]                
  ,[GuestId]                
  ,[BookingDate]                
  ,[BookingStart]                
  ,[BookingEnd]                
  ,[Amount]                
  ,[LastModifiedBy]                
  ,[LastModifiedDate]                
  ,[CreatedBy]                
  ,[CreatedDate]                
  ,[RefNumber]            
  ,[Status]       
  ,[PromoCodeId],  
  [LicPlate]      
           
  )                
 VALUES                
  (@Name,                
  @NewResourcePricingId,                
  @UserId,                
  @GuestId,                
  @BookingDate,                
  @BookingStart,                
  @BookingEnd,                
  @Amount,                
  @UserId,                
  (SELECT [dbo].[dReturnDate]( GetUTCdate())),                
  @CreatedBy,                
  (SELECT [dbo].[dReturnDate]( GetUTCdate())),                  
  '',            
  @Status,      
  @PromoCodeId,  
  @LicPlate       
   )                
                 
 SELECT SCOPE_IDENTITY()           
END    
ELSE    
 SELECT 0    
     
 COMMIT TRANSACTION TranBookingInsert    
END TRY    
BEGIN CATCH    
 ROLLBACK TRANSACTION TranBookingInsert    
 SET @ErrorMsg = ERROR_MESSAGE()    
 RAISERROR(@ErrorMsg,16,1)    
 RETURN    
END CATCH